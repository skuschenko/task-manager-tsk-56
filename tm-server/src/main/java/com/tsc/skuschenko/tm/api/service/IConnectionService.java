package com.tsc.skuschenko.tm.api.service;


import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManagerFactory;

public interface IConnectionService {

    @NotNull EntityManagerFactory getHibernateSqlFactory();

}
