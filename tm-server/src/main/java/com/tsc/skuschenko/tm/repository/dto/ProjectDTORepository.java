package com.tsc.skuschenko.tm.repository.dto;

import com.tsc.skuschenko.tm.api.repository.dto.IProjectDTORepository;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import lombok.NoArgsConstructor;
import org.hibernate.jpa.QueryHints;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.TypedQuery;
import java.util.List;

@Repository
@Scope("prototype")
@NoArgsConstructor
public final class ProjectDTORepository
        extends AbstractDTORepository<ProjectDTO>
        implements IProjectDTORepository {

    public ProjectDTORepository(
            @NotNull @Autowired EntityManager entityManager
    ) {
        this.entityManager = entityManager;
    }

    @Override
    public void clear(@NotNull String userId) {
        @NotNull final String query =
                "DELETE FROM ProjectDTO e WHERE e.userId = :userId";
        entityManager.createQuery(query)
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clearAllProjects() {
        @NotNull final String query =
                "DELETE FROM ProjectDTO e";
        System.out.println(entityManager.hashCode());
        entityManager.createQuery(query).executeUpdate();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll() {
        @NotNull final String query =
                "SELECT e FROM ProjectDTO e ";
        return entityManager.createQuery(query, ProjectDTO.class)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAllWithUserId(@NotNull String userId) {
        @NotNull final String query =
                "SELECT e FROM ProjectDTO e WHERE e.userId = :userId";
        return entityManager.createQuery(query, ProjectDTO.class)
                .setParameter("userId", userId)
                .setHint(QueryHints.HINT_CACHEABLE, true)
                .getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@NotNull String id) {
        return entityManager.find(ProjectDTO.class, id);
    }

    @Nullable
    @Override
    public ProjectDTO findOneById(@NotNull String userId, @NotNull String id) {
        @NotNull final String query =
                "SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        TypedQuery<ProjectDTO> typedQuery =
                entityManager.createQuery(query, ProjectDTO.class)
                        .setParameter("id", id)
                        .setParameter("userId", userId)
                        .setHint(QueryHints.HINT_CACHEABLE, true);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByIndex(
            @NotNull String userId, @NotNull Integer index
    ) {
        @NotNull final String query =
                "SELECT e FROM ProjectDTO e WHERE e.userId = :userId";
        TypedQuery<ProjectDTO> typedQuery =
                entityManager.createQuery(query, ProjectDTO.class)
                        .setParameter("userId", userId).
                        setFirstResult(index);
        return getEntity(typedQuery);
    }

    @Nullable
    @Override
    public ProjectDTO findOneByName(
            @NotNull String userId, @NotNull String name
    ) {
        @NotNull final String query =
                "SELECT e FROM ProjectDTO e WHERE e.userId = :userId AND " +
                        "e.name = :name";
        TypedQuery<ProjectDTO> typedQuery =
                entityManager.createQuery(query, ProjectDTO.class)
                        .setParameter("name", name)
                        .setParameter("userId", userId);
        return getEntity(typedQuery);
    }

    @Override
    public void removeOneById(
            @NotNull String userId, @NotNull String id
    ) {
        @NotNull final String query =
                "DELETE FROM ProjectDTO e WHERE e.userId = :userId AND " +
                        "e.id = :id";
        entityManager.createQuery(query)
                .setParameter("userId", userId)
                .setParameter("id", id).executeUpdate();
    }

}