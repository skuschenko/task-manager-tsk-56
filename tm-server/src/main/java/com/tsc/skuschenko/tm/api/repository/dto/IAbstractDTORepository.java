package com.tsc.skuschenko.tm.api.repository.dto;

import com.tsc.skuschenko.tm.dto.AbstractEntityDTO;
import org.jetbrains.annotations.NotNull;

import javax.persistence.TypedQuery;

public interface IAbstractDTORepository<E extends AbstractEntityDTO> {

    void add(E entity);

    E getEntity(@NotNull TypedQuery<E> query);

    void remove(E entity);

    void update(E entity);

}
