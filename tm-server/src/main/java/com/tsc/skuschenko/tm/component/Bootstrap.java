package com.tsc.skuschenko.tm.component;

import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.api.service.dto.IProjectDTOService;
import com.tsc.skuschenko.tm.api.service.dto.ITaskDTOService;
import com.tsc.skuschenko.tm.api.service.dto.IUserDTOService;
import com.tsc.skuschenko.tm.dto.ProjectDTO;
import com.tsc.skuschenko.tm.dto.TaskDTO;
import com.tsc.skuschenko.tm.dto.UserDTO;
import com.tsc.skuschenko.tm.endpoint.AbstractEndpoint;
import com.tsc.skuschenko.tm.enumerated.Role;
import com.tsc.skuschenko.tm.util.SystemUtil;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.activemq.broker.BrokerService;
import org.apache.activemq.usage.SystemUsage;
import org.apache.log4j.BasicConfigurator;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

@Getter
@Setter
@Component
@NoArgsConstructor
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String ENDPOINT_PATH =
            "com.tsc.skuschenko.tm.endpoint";

    @NotNull
    private static final String OPERATION_FAIL = "fail";

    @NotNull
    private static final String OPERATION_OK = "ok";

    @NotNull
    private static final String TM_PID = "task-manager-server.pid";

    @NotNull
    @Autowired
    private AbstractEndpoint[] abstractEndpoints;

    @NotNull
    @Autowired
    private JmsMessageComponent context;

    @NotNull
    @Autowired
    private IProjectDTOService projectDTOService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @NotNull
    @Autowired
    private ITaskDTOService taskDTOService;
    
    @NotNull
    @Autowired
    private IUserDTOService userDTOService;

    private void createDefaultProjectAndTask(
            @NotNull final UserDTO user, @NotNull final String name,
            @NotNull String description
    ) {
        @NotNull final ProjectDTO project =
                projectDTOService.add(user.getId(), name, description);
        project.setUserId(user.getId());
        @NotNull final TaskDTO task =
                taskDTOService.add(user.getId(), name, description);
        task.setProjectId(project.getId());
        task.setUserId(user.getId());
    }

    @SneakyThrows
    @Override
    public void exit() {
        context.shutdown();
        System.exit(0);
    }

    public void init() {
        initJMSBroker();
        initUsers();
        initPID();
        initEndpoints();
    }

    private void initEndpoints() {
        Arrays.stream(abstractEndpoints)
                .sorted(Comparator.comparing(item ->
                        item.getClass().getSimpleName()
                )).forEach(this::registry);
    }

    @SneakyThrows
    public void initJMSBroker() {
        BasicConfigurator.configure();
        @NotNull final BrokerService brokerService = new BrokerService();
        brokerService.addConnector("tcp://localhost:61616");
        brokerService.setPersistent(false);
        SystemUsage systemUsage = brokerService.getSystemUsage();
        systemUsage.getStoreUsage().setLimit(1024 * 1024 * 8);
        systemUsage.getTempUsage().setLimit(1024 * 1024 * 8);
        brokerService.start();
        context.run();
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = TM_PID;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void initUsers() {
        @Nullable final UserDTO userTestFind =
                userDTOService.findByLogin("test");
        if (!Optional.ofNullable(userTestFind).isPresent()) {
            @NotNull final UserDTO userTest =
                    userDTOService.create("test", "test", "text@test.ru");
            createDefaultProjectAndTask(userTest, "Test", "Test");
        }
        @Nullable final UserDTO userAdminFind =
                userDTOService.findByLogin("admin");
        if (!Optional.ofNullable(userAdminFind).isPresent()) {
            @NotNull final UserDTO userAdmin =
                    userDTOService.create("admin", "admin", Role.ADMIN);
            createDefaultProjectAndTask(userAdmin, "Admin", "Admin");
        }
    }

    private void registry(@Nullable final AbstractEndpoint endpoint) {
        if (!Optional.ofNullable(endpoint).isPresent()) return;
        @Nullable String host = propertyService.getServerHost();
        if (!Optional.ofNullable(host).isPresent()) return;
        @Nullable String port = propertyService.getServerPort();
        if (!Optional.ofNullable(port).isPresent()) return;
        @NotNull final String name = endpoint.getClass().getSimpleName();
        @NotNull final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

}
