package com.tsc.skuschenko.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface ILogService {

    void command(@NotNull String message);

    void debug(@NotNull String message);

    void error(@NotNull Exception e);

    void info(@NotNull String message);

}
