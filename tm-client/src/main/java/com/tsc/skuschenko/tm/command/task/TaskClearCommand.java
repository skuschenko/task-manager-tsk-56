package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.TaskEndpoint;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class TaskClearCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "clear all tasks";

    private static final String NAME = "task-clear";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        taskEndpoint.clearTask(session);
    }

    @Override
    public String name() {
        return NAME;
    }

}
