package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Component;

@Component
public final class ProgramExitCommand extends AbstractCommand {

    private static final String DESCRIPTION = "exit";

    private static final String NAME = "exit";

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.exit(0);
    }

    @Override
    public String name() {
        return NAME;
    }

}
