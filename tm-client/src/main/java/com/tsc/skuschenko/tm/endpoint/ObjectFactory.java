package com.tsc.skuschenko.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each
 * Java content interface and Java element interface
 * generated in the com.tsc.skuschenko.tm.endpoint package.
 * <p>An ObjectFactory allows you to programatically
 * construct new instances of the Java representation
 * for XML content. The Java representation of XML
 * content can consist of schema derived interfaces
 * and classes representing the binding of schema
 * type definitions, element declarations and model
 * groups.  Factory methods for each of these are
 * provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "createUserResponse");
    private final static QName _CreateUserWithEmailResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "createUserWithEmailResponse");
    private final static QName _CreateUserWithEmail_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "createUserWithEmail");
    private final static QName _CreateUserWithRoleResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "createUserWithRoleResponse");
    private final static QName _CreateUserWithRole_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "createUserWithRole");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "createUser");
    private final static QName _FindUserByEmailResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "findUserByEmailResponse");
    private final static QName _FindUserByEmail_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "findUserByEmail");
    private final static QName _FindUserByLoginResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "findUserByLoginResponse");
    private final static QName _FindUserByLogin_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "findUserByLogin");
    private final static QName _GetUserResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "getUserResponse");
    private final static QName _GetUser_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "getUser");
    private final static QName _IsEmailExistResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "isEmailExistResponse");
    private final static QName _IsEmailExist_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "isEmailExist");
    private final static QName _IsLoginExistResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "isLoginExistResponse");
    private final static QName _IsLoginExist_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "isLoginExist");
    private final static QName _LockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "lockUserByLoginResponse");
    private final static QName _LockUserByLogin_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "lockUserByLogin");
    private final static QName _RemoveUserByLoginResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "removeUserByLoginResponse");
    private final static QName _RemoveUserByLogin_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "removeUserByLogin");
    private final static QName _SetPasswordResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "setPasswordResponse");
    private final static QName _SetPassword_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "setPassword");
    private final static QName _UnlockUserByLoginResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "unlockUserByLoginResponse");
    private final static QName _UnlockUserByLogin_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "unlockUserByLogin");
    private final static QName _UpdateUserResponse_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "updateUserResponse");
    private final static QName _UpdateUser_QNAME = new QName("http://endpoint.tm.skuschenko.tsc.com/", "updateUser");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.tsc.skuschenko.tm.endpoint
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link CreateUser }
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link CreateUserWithEmail }
     */
    public CreateUserWithEmail createCreateUserWithEmail() {
        return new CreateUserWithEmail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmail }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "createUserWithEmail")
    public JAXBElement<CreateUserWithEmail> createCreateUserWithEmail(CreateUserWithEmail value) {
        return new JAXBElement<CreateUserWithEmail>(_CreateUserWithEmail_QNAME, CreateUserWithEmail.class, null, value);
    }

    /**
     * Create an instance of {@link CreateUserWithEmailResponse }
     */
    public CreateUserWithEmailResponse createCreateUserWithEmailResponse() {
        return new CreateUserWithEmailResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithEmailResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "createUserWithEmailResponse")
    public JAXBElement<CreateUserWithEmailResponse> createCreateUserWithEmailResponse(CreateUserWithEmailResponse value) {
        return new JAXBElement<CreateUserWithEmailResponse>(_CreateUserWithEmailResponse_QNAME, CreateUserWithEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link CreateUserWithRole }
     */
    public CreateUserWithRole createCreateUserWithRole() {
        return new CreateUserWithRole();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRole }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "createUserWithRole")
    public JAXBElement<CreateUserWithRole> createCreateUserWithRole(CreateUserWithRole value) {
        return new JAXBElement<CreateUserWithRole>(_CreateUserWithRole_QNAME, CreateUserWithRole.class, null, value);
    }

    /**
     * Create an instance of {@link CreateUserWithRoleResponse }
     */
    public CreateUserWithRoleResponse createCreateUserWithRoleResponse() {
        return new CreateUserWithRoleResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserWithRoleResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "createUserWithRoleResponse")
    public JAXBElement<CreateUserWithRoleResponse> createCreateUserWithRoleResponse(CreateUserWithRoleResponse value) {
        return new JAXBElement<CreateUserWithRoleResponse>(_CreateUserWithRoleResponse_QNAME, CreateUserWithRoleResponse.class, null, value);
    }

    /**
     * Create an instance of {@link FindUserByEmail }
     */
    public FindUserByEmail createFindUserByEmail() {
        return new FindUserByEmail();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByEmail }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "findUserByEmail")
    public JAXBElement<FindUserByEmail> createFindUserByEmail(FindUserByEmail value) {
        return new JAXBElement<FindUserByEmail>(_FindUserByEmail_QNAME, FindUserByEmail.class, null, value);
    }

    /**
     * Create an instance of {@link FindUserByEmailResponse }
     */
    public FindUserByEmailResponse createFindUserByEmailResponse() {
        return new FindUserByEmailResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByEmailResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "findUserByEmailResponse")
    public JAXBElement<FindUserByEmailResponse> createFindUserByEmailResponse(FindUserByEmailResponse value) {
        return new JAXBElement<FindUserByEmailResponse>(_FindUserByEmailResponse_QNAME, FindUserByEmailResponse.class, null, value);
    }

    /**
     * Create an instance of {@link FindUserByLogin }
     */
    public FindUserByLogin createFindUserByLogin() {
        return new FindUserByLogin();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "findUserByLogin")
    public JAXBElement<FindUserByLogin> createFindUserByLogin(FindUserByLogin value) {
        return new JAXBElement<FindUserByLogin>(_FindUserByLogin_QNAME, FindUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link FindUserByLoginResponse }
     */
    public FindUserByLoginResponse createFindUserByLoginResponse() {
        return new FindUserByLoginResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "findUserByLoginResponse")
    public JAXBElement<FindUserByLoginResponse> createFindUserByLoginResponse(FindUserByLoginResponse value) {
        return new JAXBElement<FindUserByLoginResponse>(_FindUserByLoginResponse_QNAME, FindUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link GetUser }
     */
    public GetUser createGetUser() {
        return new GetUser();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "getUser")
    public JAXBElement<GetUser> createGetUser(GetUser value) {
        return new JAXBElement<GetUser>(_GetUser_QNAME, GetUser.class, null, value);
    }

    /**
     * Create an instance of {@link GetUserResponse }
     */
    public GetUserResponse createGetUserResponse() {
        return new GetUserResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link GetUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "getUserResponse")
    public JAXBElement<GetUserResponse> createGetUserResponse(GetUserResponse value) {
        return new JAXBElement<GetUserResponse>(_GetUserResponse_QNAME, GetUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link IsEmailExist }
     */
    public IsEmailExist createIsEmailExist() {
        return new IsEmailExist();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsEmailExist }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "isEmailExist")
    public JAXBElement<IsEmailExist> createIsEmailExist(IsEmailExist value) {
        return new JAXBElement<IsEmailExist>(_IsEmailExist_QNAME, IsEmailExist.class, null, value);
    }

    /**
     * Create an instance of {@link IsEmailExistResponse }
     */
    public IsEmailExistResponse createIsEmailExistResponse() {
        return new IsEmailExistResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsEmailExistResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "isEmailExistResponse")
    public JAXBElement<IsEmailExistResponse> createIsEmailExistResponse(IsEmailExistResponse value) {
        return new JAXBElement<IsEmailExistResponse>(_IsEmailExistResponse_QNAME, IsEmailExistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link IsLoginExist }
     */
    public IsLoginExist createIsLoginExist() {
        return new IsLoginExist();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsLoginExist }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "isLoginExist")
    public JAXBElement<IsLoginExist> createIsLoginExist(IsLoginExist value) {
        return new JAXBElement<IsLoginExist>(_IsLoginExist_QNAME, IsLoginExist.class, null, value);
    }

    /**
     * Create an instance of {@link IsLoginExistResponse }
     */
    public IsLoginExistResponse createIsLoginExistResponse() {
        return new IsLoginExistResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsLoginExistResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "isLoginExistResponse")
    public JAXBElement<IsLoginExistResponse> createIsLoginExistResponse(IsLoginExistResponse value) {
        return new JAXBElement<IsLoginExistResponse>(_IsLoginExistResponse_QNAME, IsLoginExistResponse.class, null, value);
    }

    /**
     * Create an instance of {@link LockUserByLogin }
     */
    public LockUserByLogin createLockUserByLogin() {
        return new LockUserByLogin();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "lockUserByLogin")
    public JAXBElement<LockUserByLogin> createLockUserByLogin(LockUserByLogin value) {
        return new JAXBElement<LockUserByLogin>(_LockUserByLogin_QNAME, LockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link LockUserByLoginResponse }
     */
    public LockUserByLoginResponse createLockUserByLoginResponse() {
        return new LockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link LockUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "lockUserByLoginResponse")
    public JAXBElement<LockUserByLoginResponse> createLockUserByLoginResponse(LockUserByLoginResponse value) {
        return new JAXBElement<LockUserByLoginResponse>(_LockUserByLoginResponse_QNAME, LockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link RemoveUserByLogin }
     */
    public RemoveUserByLogin createRemoveUserByLogin() {
        return new RemoveUserByLogin();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "removeUserByLogin")
    public JAXBElement<RemoveUserByLogin> createRemoveUserByLogin(RemoveUserByLogin value) {
        return new JAXBElement<RemoveUserByLogin>(_RemoveUserByLogin_QNAME, RemoveUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link RemoveUserByLoginResponse }
     */
    public RemoveUserByLoginResponse createRemoveUserByLoginResponse() {
        return new RemoveUserByLoginResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "removeUserByLoginResponse")
    public JAXBElement<RemoveUserByLoginResponse> createRemoveUserByLoginResponse(RemoveUserByLoginResponse value) {
        return new JAXBElement<RemoveUserByLoginResponse>(_RemoveUserByLoginResponse_QNAME, RemoveUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link Session }
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link SetPassword }
     */
    public SetPassword createSetPassword() {
        return new SetPassword();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPassword }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "setPassword")
    public JAXBElement<SetPassword> createSetPassword(SetPassword value) {
        return new JAXBElement<SetPassword>(_SetPassword_QNAME, SetPassword.class, null, value);
    }

    /**
     * Create an instance of {@link SetPasswordResponse }
     */
    public SetPasswordResponse createSetPasswordResponse() {
        return new SetPasswordResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link SetPasswordResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "setPasswordResponse")
    public JAXBElement<SetPasswordResponse> createSetPasswordResponse(SetPasswordResponse value) {
        return new JAXBElement<SetPasswordResponse>(_SetPasswordResponse_QNAME, SetPasswordResponse.class, null, value);
    }

    /**
     * Create an instance of {@link UnlockUserByLogin }
     */
    public UnlockUserByLogin createUnlockUserByLogin() {
        return new UnlockUserByLogin();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLogin }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "unlockUserByLogin")
    public JAXBElement<UnlockUserByLogin> createUnlockUserByLogin(UnlockUserByLogin value) {
        return new JAXBElement<UnlockUserByLogin>(_UnlockUserByLogin_QNAME, UnlockUserByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link UnlockUserByLoginResponse }
     */
    public UnlockUserByLoginResponse createUnlockUserByLoginResponse() {
        return new UnlockUserByLoginResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UnlockUserByLoginResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "unlockUserByLoginResponse")
    public JAXBElement<UnlockUserByLoginResponse> createUnlockUserByLoginResponse(UnlockUserByLoginResponse value) {
        return new JAXBElement<UnlockUserByLoginResponse>(_UnlockUserByLoginResponse_QNAME, UnlockUserByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link UpdateUser }
     */
    public UpdateUser createUpdateUser() {
        return new UpdateUser();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUser }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "updateUser")
    public JAXBElement<UpdateUser> createUpdateUser(UpdateUser value) {
        return new JAXBElement<UpdateUser>(_UpdateUser_QNAME, UpdateUser.class, null, value);
    }

    /**
     * Create an instance of {@link UpdateUserResponse }
     */
    public UpdateUserResponse createUpdateUserResponse() {
        return new UpdateUserResponse();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserResponse }{@code >}}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.skuschenko.tsc.com/", name = "updateUserResponse")
    public JAXBElement<UpdateUserResponse> createUpdateUserResponse(UpdateUserResponse value) {
        return new JAXBElement<UpdateUserResponse>(_UpdateUserResponse_QNAME, UpdateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link User }
     */
    public User createUser() {
        return new User();
    }

}
