package com.tsc.skuschenko.tm.command.system;

import com.tsc.skuschenko.tm.api.service.ICommandService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.Optional;

@Component
public final class AllCommandsShowCommand extends AbstractCommand {

    private static final String DESCRIPTION = "commands";

    private static final String NAME = "commands";
    @Autowired
    private ICommandService commandService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        @NotNull final Collection<String> names =
                commandService.getListCommandNames();
        names.stream().filter(item -> Optional.ofNullable(item).isPresent())
                .forEach(System.out::println);
    }

    @Override
    public String name() {
        return NAME;
    }

}
