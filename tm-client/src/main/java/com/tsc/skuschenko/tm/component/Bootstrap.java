package com.tsc.skuschenko.tm.component;

import com.tsc.skuschenko.tm.api.service.ICommandService;
import com.tsc.skuschenko.tm.api.service.ILogService;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.api.service.IServiceLocator;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import com.tsc.skuschenko.tm.exception.system.UnknownArgumentException;
import com.tsc.skuschenko.tm.exception.system.UnknownCommandException;
import com.tsc.skuschenko.tm.util.SystemUtil;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import lombok.Getter;
import lombok.Setter;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Optional;

@Getter
@Setter
@Component
public final class Bootstrap implements IServiceLocator {

    @NotNull
    private static final String COMMAND_PATH = "com.tsc.skuschenko.tm.command";

    @NotNull
    private static final String OPERATION_FAIL = "fail";

    @NotNull
    private static final String OPERATION_OK = "ok";

    @NotNull
    private static final String TM_PID = "task-manager.pid";

    @NotNull
    @Autowired
    private AbstractCommand[] abstractCommands;

    @NotNull
    @Autowired
    private ICommandService commandService;

    @NotNull
    @Autowired
    private ILogService logService;

    @NotNull
    @Autowired
    private IPropertyService propertyService;

    @Override
    public void exit() {
        System.exit(0);
    }

    private void init() {
        initCommands();
        initPID();
    }

    private void initCommands() {
        Arrays.stream(abstractCommands)
                .sorted(Comparator.comparing(item ->
                        item.getClass().getSimpleName()))
                .forEach(this::registry);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = TM_PID;
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    public void parseArg(@Nullable final String arg) {
        if (!Optional.ofNullable(arg).isPresent()) return;
        @Nullable final AbstractCommand abstractCommand
                = commandService.getCommandByArg(arg);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownArgumentException(arg));
        abstractCommand.execute();
    }

    public boolean parseArgs(@Nullable final String[] args) {
        if (!Optional.ofNullable(args).filter(item -> item.length > 1)
                .isPresent()) {
            return false;
        }
        @Nullable final String arg = args[0];
        parseArg(arg);
        return true;
    }

    public void parseCommand(@Nullable final String command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        @Nullable final AbstractCommand abstractCommand
                = commandService.getCommandByName(command);
        Optional.ofNullable(abstractCommand)
                .orElseThrow(() -> new UnknownCommandException(command));
        abstractCommand.execute();
    }

    private void registry(@Nullable final AbstractCommand command) {
        if (!Optional.ofNullable(command).isPresent()) return;
        command.setServiceLocator(this);
        commandService.add(command);
    }

    public void run(@Nullable final String... args) {
        logService.debug("TEST");
        logService.info("***Welcome to task manager***");
        init();
        if (parseArgs(args)) exit();
        while (true) {
            try {
                System.out.print("Enter command:");
                @NotNull final String command = TerminalUtil.nextLine();
                logService.command(command);
                parseCommand(command);
                System.out.println("[" + OPERATION_OK.toUpperCase() + "]");
            } catch (@NotNull final Exception e) {
                logService.error(e);
                System.out.println("[" + OPERATION_FAIL.toUpperCase() + "]");
            }
        }
    }

}
