package com.tsc.skuschenko.tm.command.task;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.endpoint.Task;
import com.tsc.skuschenko.tm.endpoint.TaskEndpoint;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.exception.entity.task.TaskNotFoundException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    private static final String DESCRIPTION = "unbind task from project";

    private static final String NAME = "task-unbind-from-project";

    @Autowired
    private ISessionService sessionService;

    @Autowired
    private TaskEndpoint taskEndpoint;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("project id");
        @NotNull final String projectId = TerminalUtil.nextLine();
        showParameterInfo("task id");
        @NotNull final String taskId = TerminalUtil.nextLine();
        @Nullable final Task task = taskEndpoint
                .unbindTaskFromProject(
                        session, projectId, taskId
                );
        Optional.ofNullable(task).orElseThrow(TaskNotFoundException::new);
        showTask(task);
    }

    @Override
    public String name() {
        return NAME;
    }

}
