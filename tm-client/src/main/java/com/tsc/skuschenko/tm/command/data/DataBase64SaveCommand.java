package com.tsc.skuschenko.tm.command.data;


import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.DataEndpoint;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class DataBase64SaveCommand extends AbstractDataCommand {

    private static final String DESCRIPTION = "save data in base64 file";

    private static final String NAME = "data-save-base64";

    @Autowired
    private DataEndpoint dataEndpoint;

    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @SneakyThrows
    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        dataEndpoint.saveDataBase64Command(session);
    }

    @Override
    public String name() {
        return NAME;
    }

    @Override
    public String[] roles() {
        return new String[]{"Administrator"};
    }

}
