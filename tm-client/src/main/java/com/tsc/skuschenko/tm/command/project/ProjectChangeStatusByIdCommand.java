package com.tsc.skuschenko.tm.command.project;

import com.tsc.skuschenko.tm.api.service.ISessionService;
import com.tsc.skuschenko.tm.endpoint.Project;
import com.tsc.skuschenko.tm.endpoint.ProjectEndpoint;
import com.tsc.skuschenko.tm.endpoint.Session;
import com.tsc.skuschenko.tm.exception.entity.project.ProjectNotFoundException;
import com.tsc.skuschenko.tm.exception.entity.session.AccessForbiddenException;
import com.tsc.skuschenko.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
public final class ProjectChangeStatusByIdCommand
        extends AbstractProjectCommand {

    private static final String DESCRIPTION = "change project by id";

    private static final String NAME = "project-change-status-by-id";
    @Autowired
    private ProjectEndpoint projectEndpoint;
    @Autowired
    private ISessionService sessionService;

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        @Nullable final Session session = sessionService.getSession();
        Optional.ofNullable(session).orElseThrow(AccessForbiddenException::new);
        showOperationInfo(NAME);
        showParameterInfo("id");
        @NotNull final String valueId = TerminalUtil.nextLine();
        @Nullable Project project = projectEndpoint
                .findProjectById(session, valueId);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        project = projectEndpoint.changeProjectStatusById(
                session, valueId, readProjectStatus()
        );
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);
        showProject(project);
    }

    @Override
    public String name() {
        return NAME;
    }

}
