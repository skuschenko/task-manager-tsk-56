package com.tsc.skuschenko.tm.command.system;

import com.jcabi.manifests.Manifests;
import com.tsc.skuschenko.tm.api.service.IPropertyService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public final class VersionShowCommand extends AbstractCommand {

    private static final String ARGUMENT = "-v";

    private static final String DESCRIPTION = "version";

    private static final String NAME = "version";
    @Autowired
    private IPropertyService service;

    @Override
    public String arg() {
        return ARGUMENT;
    }

    @Override
    public String description() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        showOperationInfo(NAME);
        System.out.println("VERSION: " + service.getApplicationVersion());
        System.out.println("BUILD: " + Manifests.read("build"));
    }

    @Override
    public String name() {
        return NAME;
    }

}