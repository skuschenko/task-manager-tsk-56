package com.tsc.skuschenko.tm.service;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.api.service.ICommandService;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.Optional;

@Service
public final class CommandService implements ICommandService {

    @NotNull
    private final ICommandRepository commandRepository;

    public CommandService(@NotNull final ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public void add(@Nullable final AbstractCommand abstractCommand) {
        Optional.ofNullable(abstractCommand)
                .ifPresent(commandRepository::add);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArgs() {
        return commandRepository.getArgs();
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return commandRepository.getArguments();
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return commandRepository.getCommandByArg(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commandRepository.getCommandByName(name);
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return commandRepository.getCommands();
    }

    @NotNull
    @Override
    public Collection<String> getListArgumentName() {
        return commandRepository.getCommandArgs();
    }

    @NotNull
    @Override
    public Collection<String> getListCommandNames() {
        return commandRepository.getCommandNames();
    }

}
