package com.tsc.skuschenko.tm.repository;

import com.tsc.skuschenko.tm.api.repository.ICommandRepository;
import com.tsc.skuschenko.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;

import java.util.*;
import java.util.stream.Collectors;

@Repository
public final class CommandRepository implements ICommandRepository {

    @NotNull
    private final Map<String, AbstractCommand> arguments =
            new LinkedHashMap<>();

    @NotNull
    private final Map<String, AbstractCommand> commands =
            new LinkedHashMap<>();

    @Override
    public void add(final AbstractCommand abstractCommand) {
        @NotNull final Optional<String> arg =
                Optional.ofNullable(abstractCommand.arg());
        @NotNull final Optional<String> name =
                Optional.ofNullable(abstractCommand.name());
        arg.ifPresent(item -> arguments.put(item, abstractCommand));
        name.ifPresent(item -> commands.put(item, abstractCommand));
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArgs() {
        return commands.values()
                .stream()
                .filter(item -> Optional.ofNullable(item.arg()).isPresent())
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getArguments() {
        return new ArrayList<>(arguments.values());
    }

    @NotNull
    @Override
    public Collection<String> getCommandArgs() {
        return commands.values()
                .stream()
                .map(AbstractCommand::arg)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public AbstractCommand getCommandByArg(@NotNull final String name) {
        return arguments.get(name);
    }

    @Nullable
    @Override
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return commands.get(name);
    }

    @NotNull
    @Override
    public Collection<String> getCommandNames() {
        return commands.values()
                .stream()
                .map(AbstractCommand::name)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());
    }

    @NotNull
    @Override
    public Collection<AbstractCommand> getCommands() {
        return new ArrayList<>(commands.values());
    }

}
